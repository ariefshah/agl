﻿using AGL.Pets.DAL;
using AGL.Pets.Data.Interfaces;
using Autofac;

namespace AGL.Pets.Console
{
    public class Bootstrap
    {
        
        public  static IContainer Start()
        {
            var builder = new ContainerBuilder();
           
            builder.RegisterType<DataContext>().As<IDataContext>();
            builder.RegisterType<PersonRepository>().As<IPersonRepository>();

            return builder.Build();
        }
    }
}
