﻿using System;
using AGL.Pets.Data.Interfaces;
using Autofac;
using System.Collections.Generic;
using AGL.Pets.Data.Entities;

namespace AGL.Pets.Console
{
    class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            Container= Bootstrap.Start();
            var repository = Container.Resolve<IPersonRepository>();
            var result = repository.GetCatsByOwnerGender();

            WritePets(result);
            System.Console.WriteLine();
            System.Console.WriteLine("Press any key to exit");
            System.Console.ReadKey();
        }
        private static void WritePets(Dictionary<string, List<Pet>> items)
        {
            foreach (var item in items)
            {
                System.Console.WriteLine("{0}", item.Key);
                foreach (var pet in item.Value)
                {
                    System.Console.WriteLine("\t{0}", pet.Name);
                }
            }
        }
    }
}
