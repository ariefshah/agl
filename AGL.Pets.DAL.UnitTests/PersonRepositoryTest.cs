﻿using System;
using AGL.Pets.Data.Interfaces;
using NUnit.Framework;
using Moq;
using AGL.Pets.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace AGL.Pets.DAL.UnitTests
{
    [TestFixture]
    public class PersonRepositoryTest
    {
        private Mock<IDataContext> _mockedDatacontext;
        private IQueryable<Person> mockperson;
        [SetUp]
        public void SetupMocks()
        {
            var mockrepo = new MockRepository(MockBehavior.Strict);
            _mockedDatacontext = mockrepo.Create<IDataContext>();
            mockperson = new List<Person> {
                    new Person
                    {
                        Gender = "Male",
                        Pets = new List<Pet>
                        {
                            new Pet
                            {
                                Type = "caT",
                                Name = "B",
                            },
                            new Pet
                            {
                                Type = "caT",
                                Name = "A",
                            },
                        }
                    },
                    new Person
                    {
                        Gender = "Female",
                        Pets = new List<Pet>
                        {
                            new Pet
                            {
                                Type = "caT",
                                Name = "C",
                            },
                            new Pet
                            {
                                Type = "whale",
                                Name = "A",
                            },
                            new Pet
                            {
                                Type = "SNAKE",
                                Name = "A",
                            },
                            new Pet
                            {
                                Type = "CATt",
                                Name = "A",
                            },
                        }

                    }
                }.AsQueryable();

        }

        [Test]
        public void SortCatsByOwnerGenderTest_Valid_Success()
        {
            //Arrange
            var repository = new PersonRepository(_mockedDatacontext.Object);
          
            _mockedDatacontext.Setup(call=>call.Persons()).Returns(mockperson);

            /// ACT
            var items = repository.GetCatsByOwnerGender();
           //Assert
            Assert.IsTrue(items.Count == 2);
            Assert.IsTrue(items.Keys.ToList()[0] == "Male");
            Assert.IsTrue(items["Male"].Count == 2);
            Assert.IsTrue(items["Male"][0].Name == "A");
            Assert.IsTrue(items["Male"][1].Name == "B");
            Assert.IsTrue(items.Keys.ToList()[1] == "Female");
            Assert.IsTrue(items["Female"].Count == 1);
            Assert.IsTrue(items["Female"][0].Name == "C");
        }
    }
}
