# AGL #

To download the repo please use

```
#!python

git clone https://ariefshah@bitbucket.org/ariefshah/agl.git

```



### Cyclometric Complexity###
![screenshot](https://bytebucket.org/ariefshah/agl/raw/88d264992ea4d96c357ab055830cb3854ec0c1b5/images/Capture.PNG?token=557dbf6625930ff2b32191eaec453c2a4ecf7e02)

### Purpose?? ###

Programming challenge

A json web service has been set up at the url: http://agl-developer-test.azurewebsites.net/people.json

You need to write some code to consume the json and output a list of all the cats in alphabetical order under a heading of the gender of their owner.

You can write it in any language you like. You can use any libraries/frameworks/SDKs you choose.

Submissions which include tests will be looked upon more favourably

Example:

Male

Angel
Molly
Tigger
Female

Gizmo
Jasper
Notes

Use of language features and/or libraries for grouping/sorting is encouraged. Eg: C# -> LINQ, Java8 -> Stream API, Javascript -> Lodash, Python -> List comprehension
Use of libraries for consumption of web services is encouraged. Eg: C# -> HttpClient, Java -> HttpClient, Javascript -> jQuery
You can write it in your favourite Text Editor/IDE
Submissions will only be accepted via github or bitbucket