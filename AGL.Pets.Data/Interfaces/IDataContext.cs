﻿using AGL.Pets.Data.Entities;
using System.Linq;

namespace AGL.Pets.Data.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDataContext
    {
        IQueryable<Person> Persons();
    }
}
