﻿using AGL.Pets.Data.Entities;
using System.Collections.Generic;

namespace AGL.Pets.Data.Interfaces
{
    public interface IPersonRepository
    {
        Dictionary<string, List<Pet>> GetCatsByOwnerGender();
    }
}
