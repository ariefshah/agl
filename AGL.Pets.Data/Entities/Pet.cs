﻿namespace AGL.Pets.Data.Entities
{
    public class Pet
    {
        public string Name { get; set; }

        public string Type { get; set; }

    }
}
