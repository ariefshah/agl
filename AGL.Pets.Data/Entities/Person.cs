﻿using System.Collections.Generic;

namespace AGL.Pets.Data.Entities
{
    public  class Person
    {
        public int Age { get; set; }

        public string Gender { get; set; }

        public string Name { get; set; }

        public ICollection<Pet> Pets { get; set; }

    }
}
