﻿using AGL.Pets.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGL.Pets.Data.Entities;
using System.Net.Http;
using Newtonsoft.Json;

namespace AGL.Pets.DAL
{
    public class DataContext : IDataContext
    {
        public IQueryable<Person> Persons()
        {
            return Task.Run(async () => await GetPersons()).Result;
        }
        private async Task<IQueryable<Person>> GetPersons()
        {
            var url = @"http://agl-developer-test.azurewebsites.net/people.json";
            using (var client = new HttpClient())
            using (var response = await client.GetAsync(url))
            using (var content = response.Content)
            {
                string json = await content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<List<Person>>(json);

                return result.AsQueryable();
            }
        }
    }
}
