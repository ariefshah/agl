﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGL.Pets.Data.Entities;
using AGL.Pets.Data.Interfaces;

namespace AGL.Pets.DAL
{
    public class PersonRepository : IPersonRepository
    {
        private readonly IDataContext _dataContext;

        public PersonRepository(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public Dictionary<string, List<Pet>> GetCatsByOwnerGender()
        {
            var query = from person in _dataContext.Persons()
                        where person.Pets != null
                        group person by person.Gender into petlist
                        select new
                        {
                            Gender = petlist.Key,
                            Pets = petlist.SelectMany(n => n.Pets)
                            .Where(q => q.Type.Equals("Cat", StringComparison.InvariantCultureIgnoreCase))
                            .OrderBy(o => o.Name)
                            .ToList(),
                        };

            var result = query.ToDictionary(n => n.Gender, n => n.Pets);

            return result;
        }
    }
}
